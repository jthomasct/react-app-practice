import React from "react";
import { motion } from "framer-motion";

const Display = (props: any) => {
  const style = {
    color: "blue",
  };

  return (
    <div className="w-32 bg-yellow-700 rounded-full">
      {props.word}
    </div>
  );
};
export default Display;
