import React from 'react';
import Greeting from "./Greeting";
import { motion } from "framer-motion"

function Sidebar() {
    return (
        <>
            <Greeting />
            <div className="sm:hidden">Goodbye!</div>
            <div className="md:hidden">Get lost!</div>
            <div className="lg:hidden">hello!</div>
            <div className="xl:hidden">foo!</div>
            <div className="2xl:hidden">furby!</div>
        </>
    );
};
export default Sidebar;
