import { stringify } from "querystring";
import React, { useState } from "react";
import Display from "./Display";
import UserGreeting from "./UserGreeting";
import GuestGreeting from "./GuestGreeting";


function Greeting() {
  const [signedIn, setSignedIn] = useState(false);
  const [greeting, setGreeting] = useState("Greetings");
  const [color, setColor] = useState(true);
  const [size, setSize] = useState("");

  const handleGreeting = (e: any) => setGreeting(e.target.value);
  const handleColor = (e: any) => setColor(e.target.value);
  //   const handleClick = () => setColor(!color)

  return (
    <div>
      <button
        style={{ display: "inline", fontSize: "20px" }}
        onClick={() => setSignedIn(!signedIn)}
      >
        click me to sign {signedIn ? "in" : "out"}
      </button>

      <input value={greeting} onChange={handleGreeting} />
      <input value={size} onChange={e => setSize(e.target.value)} />
      {/* <input value={color} onChange={handleColor} /> */}
      <button onClick={() => setColor(!color)}>click me to change color</button>
      <Display size={size} word={greeting} color={color} />
      {signedIn ? <UserGreeting /> : <GuestGreeting />}
    </div>
  );
}
export default Greeting;
