import React from "react";
import { motion } from "framer-motion";

const UserGreeting = () => {
  return <motion.div initial={{opacity: 0}} animate={{opacity: 1}}>"Welcome Back!"</motion.div>;
};
export default UserGreeting;
