import React from 'react';
import { motion } from "framer-motion";

const GuestGreeting = () => {
    return <motion.div initial={{opacity: 0}} animate={{opacity: 1}}>"Hello!"</motion.div>;
  };
  export default GuestGreeting
