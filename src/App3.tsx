import "./css/index.css";
import "./css/tailwind.css";
import { set1, set2 } from "./utils/data";
import { useState } from "react";

function matcher(a: string[], b: string[]): string[] {
    return a.filter(i => {
        for (const j of b) {
            if (i === j) {
                return true;
            }
        }
        return false;
    });
}

const App3 = () => {
  const [ result, setResult ] = useState(matcher(set1, set2));
  return (
    <div className="w-4/5 mx-auto bg-blue-100">
      <div className="text-5xl text-center">contents:</div>
        <div className="flex justify-center">
            <div className="w-1/3 bg-red-100 text-center">
                {result.map((i) => (
                    <div>{i}</div>
                ))}
            </div>
        </div>
      <div className="flex justify-around text-xs h-screen">
        <div className="flex flex-col justify-center h-full w-1/3 bg-red-100 text-center">
          {set1.map((i) => (
            <div className="my-3">{i}</div>
          ))}
        </div>
        <div className="flex flex-col justify-center h-full w-1/3 bg-yellow-100 text-center">
          {set2.map((i) => (
            <div className="my-3">{i}</div>
          ))}
        </div>
      </div>
    </div>
  );
};
export default App3;
