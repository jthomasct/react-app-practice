import React from 'react';
import logo from "./logo-1.svg";
// import './App.css';
import Sidebar from './components/Sidebar'

function App() {
  return (
    <div className="App w-3/4 bg-purple-100 sm:bg-pink-800 md:bg-yellow-100 lg:bg-blue-300 mx-auto text-center rounded-3xl rounded-t-none">
      <header className="App-header">
        <Sidebar />
        <img src={logo} className="w-1/4 mx-auto" alt="logo" />
        <p className="text-right">
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
};

export default App;
