import React from 'react';
import logo from "./logo.jpg";

function App2() {
    return (
        <div className="w-screen h-screen bg-cover bg-center bg-no-repeat" style={{backgroundImage: `url(${logo})`}}>
            <p className="text-right text-6xl text-red-500">
                <br/>
                <div className="flex">
                    <div className="flex-1 text-center">:</div>
                    <div className="flex-1 text-center minWidth-3/5"></div>
                    <div className="flex-1 text-center">:</div>
                </div>
                <div className="flex">
                    <div className="flex-1 text-center">:</div>
                    <div className="flex-1 text-center minWidth-3/5"></div>
                    <div className="flex-1 text-center">:</div>
                </div>
                <div className="flex">
                    <div className="flex-1 text-center">:</div>
                    <div className="flex-1 text-center minWidth-3/5"></div>
                    <div className="flex-1 text-center">:</div>
                </div>
                <div className="flex">
                    <div className="flex-1 text-center">:</div>
                    <div className="flex-1 text-center minWidth-3/5"></div>
                    <div className="flex-1 text-center">:</div>
                </div>
                <div className="flex">
                    <div className="flex-1 text-center">:</div>
                    <div className="flex-1 text-center minWidth-3/5"></div>
                    <div className="flex-1 text-center">:</div>
                </div>
                <br/>
            </p>
        </div>
    )
}
export default App2
