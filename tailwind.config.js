module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'corechain': '#40D8C6'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
